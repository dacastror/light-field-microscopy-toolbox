function guardar_imagen(varargin)
    
    campo = varargin{1};
    name = varargin{2};
    bits = varargin{3};
    campo = (campo-min(campo(:)))/(max(campo(:))-min(campo(:)));
    if bits == 8
        campo = uint8(campo*(2^bits-1));
    elseif bits == 16
        campo = uint16(campo*(2^bits-1));
    elseif bits == 32
        campo = uint32(campo*(2^bits-1));
    end
    if nargin > 3
        texto = varargin{4};
        imwrite(campo,name,'Description',texto);
        [ruta,nombre,~] = fileparts(name);
        nametext = fullfile(ruta,strcat(nombre, '(parámetros).txt'))
        fileID = fopen(nametext,'w');
        fprintf(fileID,'%s\n',texto);
        fclose(fileID);
    else
        imwrite(campo,name)
    end
end