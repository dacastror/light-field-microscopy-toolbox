function valcoef = ajuste_lorentz(datos)
    lorentzEqn = 'a*(c/(4*(x-b)^2+c*c))+d';
    % significados: a = pi*area/2, b = centro, c = FWHM, d = offset en y
    % Altura de la curva (en yc - y0) es; H = a/c

    % [valMax,indic] = max(stack(:));
    % [indi_max_x, indi_max_y, indi_max_z] = ind2sub(size(stack),indic);
    % datos = squeeze(stack(indi_max_x, indi_max_y,:));
    plot(datos)
    d_z = 1:length(datos);
    [valMax,indi_max_z] = max(datos(:));
    valMin = min(datos(:));
    midadAlto = (valMax-valMin)*0.5+valMin;
    [~,indiMitad] = min(abs(datos-midadAlto));
    ancho = 2*abs(indiMitad-indi_max_z);
    startPoints = [valMax*ancho indi_max_z ancho valMin ];
    

    
    
    [f1, d1] = fit(d_z',datos,lorentzEqn,'Start', startPoints);
    valcoef = coeffvalues(f1);
    % centro = valcoef(2)
    plot(f1,d_z,datos)
    % valcoef(3)
end