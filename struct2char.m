function texto = struct2char(structu)
    query = fieldnames(structu);
%     keys = pad(cellstr(query),'left');
    keys = cellstr(query);
    vals = cellfun(@(f) strcat(" = ",string(structu.(f)),", "), query, 'uni',0);
    values = cat(1, vals{:});
    vector = char(strcat(keys, values));
    texto = '';
    for i=1:size(vector,1); texto = [texto vector(i,:)]; end
    texto = regexprep(texto, ' +', ' ');
    % b = char(strjoin(a))
end
