folderInput = 'Stacks-simulados';
folderOut = 'campos-de-luz';
nameOut = 'LF_simulado_';  % este nombre se concatenara con nameStack
nameStack = 'letras_ABC.tif';

% parametros de la simulacion
P = struct;  % es util para limpiar la variable P y evitar conflictos
P.paso = 0.5;
P.desplazadoZ = 0;

% ruta del stack de entrada
fname = fullfile(folderInput,nameStack);
infoima = imfinfo(fname);
bits = infoima.BitDepth;
% simular campo de luz
[campo,P] = simulador_light_field(fname,P);
metaData = struct2char(P);
% formato = '%s_(paso %0.3fum)(desplazadoZ %0.3fum)';
% nameOut = sprintf(formato,nameOut,P.paso,P.desplazadoZ);
nameCampo = fullfile(folderOut,sprintf('%s__%s',nameOut,nameStack));
guardar_imagen(campo,nameCampo,bits,metaData);

% mostrar campo resultante
figure(1)
imshow(campo,[])
