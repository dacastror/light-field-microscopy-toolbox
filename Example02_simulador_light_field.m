% tamano del stack simulado %
sizex = 1080;
sizey = 1920;
sizez = 2;

% parametros de la simulacion
P = struct;  % es util para limpiar la variable P y evitar conflictos
P.paso = 0.4;
P.lentesx = 100;
P.lentesy = 120;
P.pix_sensor = 20;
P.size_lente = 131;
P.magnificacion = 40;
P.apertura_numerica = 1.05;
P.indice_refraccion = 1.5;
P.muestreo = 4;
P.desplazadoZ = 10;

% crear stack simulado
stack = zeros(sizex,sizey,sizez);
stack(round(sizex*0.5),round(sizey*0.5),:) = 2^16-1;

% simular campo de luz
[campo,~] = simulador_light_field(stack,P);

% mostrar campo resultante
figure(1)
imshow(campo,[])
