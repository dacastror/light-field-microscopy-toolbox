function verParcelacion(ima,periodo)
    % Copyright (C) 2018 Diego Alberto Castro Rodriguez <dacastror@gmail.com>
    % License: MIT
    % -------------------------------------------------------------------

    figure(3); imshow(ima,[])
    hold on
    drawVerticalLines(ima,periodo);
    drawHorizontalLines(ima,periodo);
    hold off
end

function drawVerticalLines(ima,periodo)
    [sizex,sizey] = size(ima);
    for i=1:round(sizey/periodo)
        ind = periodo*i+0.5;
        line([ind,ind],[0.5,sizex+0.5],'Color','red')
    end
end

function drawHorizontalLines(ima,periodo)
    [sizex,sizey] = size(ima);
    for i=1:round(sizex/periodo)
        ind = periodo*i+0.5;
        line([0.5,sizey+0.5],[ind,ind],'Color','red')
    end
end