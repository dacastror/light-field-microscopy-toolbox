folderOut = 'C:\Users\da.castrom\Downloads';
namestackOut = 'test';

% tamano del stack simulado %
sizex = 2047;
sizey = 2047;
sizez = 1;

% parametros de la simulacion
P = struct;  % es util para limpiar la variable P y evitar conflictos
P.paso = 0.3;
P.muestreo = 3;
P.desplazadoZ = 5;
P.MostrarBarraProgreso = false;
P.simularApertura = true;
P.pix_sensor = 20;

% crear stack simulado
stack = zeros(sizex,sizey,sizez);
n = 5;
mitadx = round(sizex*0.5);
mitady = round(sizey*0.5);
stack(mitadx-n:mitadx+n,mitady-n:mitady+n) = 2^16-1;

alphas = 1:0.05:2;
% simular campo de luz
[campo,P] = simulador_light_field(stack,P);

tensor = LightFieldToTensor(campo,P.pix_sensor);
% perspectivaInteractiva(tensor)

stack = stackReenfoque(tensor,alphas,'linear');

visualizador_stack(stack,'stack');
% guardarStackTif(stack,folderOut,namestackOut)
% % mostrar campo resultante
figure(3)
imshow(campo,[])



