function [campo,P] = simulador_light_field(stack,parametros)
    % Copyright (C) 2018 Diego Alberto Castro Rodriguez <dacastror@gmail.com> 
    % version: 0.3.1
    % License: MIT
    %
    % La configuracion del microscopio esta basada en el diseno descrito en
    % Levoy, M. (2006). Optical recipes for light field microscopes Optical
    % recipes for light field microscopes. Stanford Computer Graphics
    % Laboratory Technical Memo 2006-001
    % -------------------------------------------------------------------
    %
    % SIMULADOR_LIGHT_FIELD simula un campo de luz a partir de un stack de
    % imagenes.
    %
    % campo = simulador_light_field(stack,parametros);
    % campo = simulador_light_field(nameStack,parametros);
    %
    % Inputs:
    %   stack: puede ser un arreglo numerico 3d o un char. 
    %   si es un char, este debe ser la ruta del stack de entrada (incluido
    %   el nombre).
    %
    %   parametros: struct con los parametros de la simulacion. Para ver 
    %   con mas detalle los marametros disponibles, ver al final de este
    %   archivo la funcion llamada "parametros_por_defecto()".
    %   El struct de entrada solo necesita contener los parametros que se
    %   deseen cambiar respecto a los valores por defecto.
    %
    % Outputs:
    %   campo: arreglo 2d tipo double del campo de luz simulado.
    %
    %   P: parametros utilizados en la simulación.

    P = parametros_de_simulacion(parametros);
    [sizex,sizey,sizez] = size_stack(stack);
    % lenMax: numero de microlentes en la direccion con mas microlentes
    lenMax = P.lentesy;
    if P.lentesx>P.lentesy; lenMax = P.lentesx; end
    % angulo de apertura del objetivo
    angulo = asin(P.apertura_numerica/P.indice_refraccion);
    distMax = tan(angulo);
    % distancia virtual entre puntos de "observacion"
    deltaXY = 2*distMax/(P.pix_sensor-1);
    % tamano de imagen en con el que se realizaran las proyecciones
    new_size = lenMax*P.muestreo;
    % comparacion entre el ratio xy del stack y el de las microlentes
    compRatios = sizey/sizex > P.lentesy/P.lentesx;
    % factor de escala y conversion de micras a pixeles
    factor_scal = new_size*P.magnificacion/(P.size_lente*lenMax);
    % paso entre imagenes en pixeles
    paso_scal = P.paso*factor_scal;
    % crear matriz de P.margen_mask para igualar el ratio del numero de 
    % microlentes con el ratio del tamano de la imagen
    if compRatios
        new_sizex = sizey*P.lentesx/P.lentesy;
        margenx = floor((new_sizex-sizex)*0.5);
        margeny = 0;
        matrizMx = zeros(margenx,sizey);
    else 
        new_sizey = sizex*P.lentesy/P.lentesx;
        margeny = floor((new_sizey-sizey)*0.5);
        margenx = 0;
        matrizMy = zeros(sizex,margeny);
    end
    % tan(angulos) de proyeccion del volumen respecto al eje optico
    tanAng = -distMax:deltaXY:distMax;
    % dist del plano focal a la primera imagen del stack de entrada (en pix)
    z_ini = -paso_scal*(sizez-1)*0.5+factor_scal*P.desplazadoZ;
    svistaX = P.lentesx*P.muestreo;
    svistaY = P.lentesy*P.muestreo;
    persph  = zeros(svistaX,svistaY*P.pix_sensor);
    mosaico = zeros(svistaX*P.pix_sensor,svistaY*P.pix_sensor);
    mosaic_total = mosaico;
    if P.MostrarBarraProgreso
        barra = waitbar(0,'calculando proyecciones del volumen...');
        tiempos = zeros(2,1);
    end
    for k = 1:sizez
        id_time = tic;
        if isnumeric(stack); Ima=stack(:,:,k);else; Ima=imread(stack,k);end
        % aplicar margenes para igualar ratios si es necesario
        if margenx>0; Ima = [matrizMx; Ima; matrizMx]; end
        if margeny>0; Ima = [matrizMy, Ima, matrizMy]; end
        Ima = imresize(Ima, [svistaX,svistaY],P.methodScal);
        kz = z_ini+paso_scal*(k-1);
        trs = kz*tanAng;
        for i = 1:P.pix_sensor
            vista_x = imtranslate(Ima,[trs(i), 0],P.method);
            persph(:,(i-1)*svistaY+1:i*svistaY) = vista_x;
        end
        for i = 1:P.pix_sensor
            vistas_y = imtranslate(persph,[0, trs(i)],P.method);
            mosaico((i-1)*svistaX+1:i*svistaX,:) = vistas_y;
        end
        mosaic_total = mosaic_total + mosaico;
        % para barra de progreso
        if P.MostrarBarraProgreso
            tiempos = progreso(barra,tiempos,k,sizez,id_time);
        end
    end
    final_size = [P.lentesx*P.pix_sensor,P.lentesy*P.pix_sensor];
    mosaic_total = imresize(mosaic_total,final_size,P.methodScal);
    if P.MostrarBarraProgreso; delete(barra); end
    campo = perspectivas2campo(mosaic_total,P);
    if P.simularApertura; campo = simular_apertura(campo,P); end
end

function campo = perspectivas2campo(mosaico,P)
    lx = P.lentesx;
    ly = P.lentesy; 
    tensor = zeros(P.lentesx,P.lentesy,P.pix_sensor,P.pix_sensor);
    for j=1:P.pix_sensor
        for i=1:P.pix_sensor
            tensor(:,:,i,j) = mosaico(1+(i-1)*lx:i*lx,1+(j-1)*ly:j*ly);
        end
    end
    campo = zeros(P.lentesx*P.pix_sensor,P.lentesy*P.pix_sensor);
    L = P.pix_sensor;
    for j = 1:P.lentesy
        for i = 1:P.lentesx
            campo(1+(i-1)*L:i*L,1+(j-1)*L:j*L) = tensor(i,j,:,:);
        end
    end
end

function [sizex,sizey,sizez] = size_stack(stack)
    % informacion del stack
    if isnumeric(stack)
        [sizex,sizey,sizez] = size(stack);
    else 
        info  = imfinfo(stack);
        sizey = info.Width;
        sizex = info.Height;
        sizez = numel(info);
    end
end

function tiempos = progreso(h,tiempos,count,sz,id_time)
    
    tiempos(count) = toc(id_time);
    part = count/sz;
    t = round(mean(tiempos)*(sz-count));
    format = 'Tiempo restante %is. (procesado %i de %i)';
    waitbar(part,h,sprintf(format,t,count,sz));
end

function campo = simular_apertura(campo,P)
    % aplicar mascara para simular la apertura del objetivo
    ns = P.pix_sensor*P.muestreo_mask;
    r = ns/2-0.5*P.margen_mask*P.muestreo_mask;
    x = linspace(0,ns,ns)-ns/2;
    [X,Y] = meshgrid(x,x);
    circ = zeros(ns,ns);
    circ(X.^2 + Y.^2<r^2) = 1;
    circ = imresize(circ,[P.pix_sensor,P.pix_sensor],'bilinear');
    circ_b = zeros(P.pix_sensor,P.pix_sensor);
    circ_b(2:end-1,2:end-1)= circ(2:end-1,2:end-1);
    mascara_c = repmat(circ_b,P.lentesx ,P.lentesy);
    campo = campo.*mascara_c;
end

function array = cell2array(cell)
    array = string(zeros(1,numel(cell)));
    for i = 1:numel(cell)  
       array(i) = string(cell{i});
    end
end

function P = parametros_de_simulacion(parametros)
    % cargar parametros por defecto
    P = parametros_por_defecto();
    % modificar los parametros por defecto con los de entrada
    fields = fieldnames(parametros);
    fields_d = fieldnames(P);
    fieldsArr = cell2array(fields);
    fields_dArr = cell2array(fields_d);
    for i = 1:numel(fields)
        val = find(fields_dArr==fieldsArr(i), 1);
        if isempty(val)
            error('El parametro "%s" no esta definido',fieldsArr(i));
        end
        P.(fields{i}) = parametros.(fields{i});
    end
end

function P = parametros_por_defecto()
    P.paso       = 0.3;  % distancia entre planos en el stack de entrada(um)
    P.lentesx    = 101;  % numero de microlentes en direccion vertical
    P.lentesy    = 101;  % numero de microlentes en direccion horizontal
    P.pix_sensor = 20;   % lado en pixeles del sensor detras de cada microlete
    P.size_lente = 130;  % diametro de cada microlente del arreglo (um)
    P.magnificacion     = 50;   % magnificacion del sistema optico
    P.apertura_numerica = 1.1;  % apertura numerica del objetivo de microscopio
    P.indice_refraccion = 1.33; % indice de refraccion del medio de inmersion
    % muestreo controla el tamano del volumen que se proyectara para crear el 
    % campo de luz, 1 significa que las imagenes de entrada se reescalan para 
    % quedar de tamano = (lentesx, lentesy), con un numero mayor a 1, el campo 
    % de luz tendra un mejor muestreo, por lo que tendra mayor calidad.
    % Este numero no altera el tamano final (alto ancho) del campo de luz.
    P.muestreo   = 3; 
    P.desplazadoZ = 0; % desplazar el volumen observado en direccion Z (um)
    P.method     = 'linear';    % metodo de interpolacion en la translacion     
    P.methodScal = 'bilinear';  % metodo de interpolacion en la escala
    P.simularApertura = true;   % simular apertura circular?
    P.margen_mask = 2;          % margen entre microlentes
    P.muestreo_mask = 8;        % muestreo para simular la apertura 
    P.MostrarBarraProgreso = true; % Mostrar la barra de progreso
end
